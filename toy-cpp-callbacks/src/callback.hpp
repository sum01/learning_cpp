#ifndef CALLBACK_HPP
#define CALLBACK_HPP

// This defines a simple, inheritable class type to allow for callbacks
namespace ksig {
	template<typename T>
	class callback {
public:
		// Allow optional data be passed (or non-optional if you throw)
		// Data could be used, for example, to pass a gamestate to an object..
		// which then evals the gamestate and performs different things based on that
		virtual void signal() = 0;
		// virtual void signal(const T *data = nullptr);
		// Signal, but also return something
		virtual T emit() = 0;
		// virtual T emit(const T *data = nullptr);
	};
}

#endif

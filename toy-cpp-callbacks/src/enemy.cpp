#include "enemy.hpp"

using namespace game;

enemy::enemy(const unsigned int &x)
	: hp(x) {
}

bool enemy::tick() noexcept {
	// While the enemy has hp, keep the callback
	// But if it died, forget the callback
	return --this->hp != 0;
}

int enemy::get_hp() const noexcept {
	return this->hp;
}

void enemy::signal() noexcept {
	this->tick();
}

bool enemy::emit() noexcept {
	return this->tick();
}

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "callback.hpp"
#include <ostream>

namespace game {
	// Inherit bool as that's all we use for emit "ok/not-ok" if() checks
	// Where true is "all good"
	class enemy : public ksig::callback<bool> {
private:
		int hp;
public:
		explicit enemy(const unsigned int & = 10);
		// Our actual callback function that both signal and emit call
		// True if the callback should be forgotten after calling it
		bool tick() noexcept;
		// Simple getter
		int get_hp() const noexcept;
		// Callback overrides
		void signal() noexcept override;
		bool emit() noexcept override;
		// Friend for easier printing
		friend std::ostream & operator<<(std::ostream &out, const enemy &e) {
			return out << e.get_hp();
		}
	};
}


#endif

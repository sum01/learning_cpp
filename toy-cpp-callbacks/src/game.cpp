#include "game.hpp"

using namespace game;

void loop::queue_callback(ksig::callback<bool> &c) noexcept {
	this->callbacks.emplace(c);
}

int loop::start() noexcept {
	// Our pretend game loop
	// Normally this would be based on some tick rate & time passed
	// Run code, then do callbacks at the end
	// some code blah blah....
	// ...
	// Check if there's anything in the callback queue
	while (this->callbacks.size() > 0) {
		// callbacks here
		ksig::callback<bool> &f = this->callbacks.front();
		// True if it should be requeued
		if (f.emit()) {
			// Push it to the back again
			this->callbacks.emplace(f);
		}
		// Remove it
		this->callbacks.pop();
	}
	// Everything was good, return a 0 like main()
	return 0;
}

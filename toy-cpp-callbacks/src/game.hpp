#ifndef GAME_HPP
#define GAME_HPP

#include "callback.hpp"

#include <queue>
#include <functional>

namespace game {
	class loop {
private:
		std::queue<std::reference_wrapper<ksig::callback<bool> > > callbacks;
public:
		// Need to be non-const refs to be movable into our queue
		void queue_callback(ksig::callback<bool> &c) noexcept;
		int start() noexcept;
	};
}

#endif

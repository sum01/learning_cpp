#include <iostream>
#include "enemy.hpp"
#include "game.hpp"

int main() {
	using namespace game;
	// Create a game with 5 enemies
	loop mygame;
	enemy a{213}, b{2}, c{1};

	std::cout << "Enemies: " << a << " " << b << " " << c << '\n';

	// Stupid example, but more realistically this would be a public method in some game class
	mygame.queue_callback(a);
	mygame.queue_callback(b);
	mygame.queue_callback(c);

	mygame.start();

	std::cout << "Do the thing? (t/f):\n";
	char do_thing;
	std::cin >> do_thing;

	if (do_thing == 't') {
		std::cout << "Enemies: " << a << " " << b << " " << c << '\n';
	}

	return 0;
}
